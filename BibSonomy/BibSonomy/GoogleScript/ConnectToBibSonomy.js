/**
 * Fetches the data from Bibsonomy.
 * @param mode The search mode to use. Valid values: own, user, public
 * @param searchTerm The search terms to filter the publications
 * @param user The user name, if mode is user
 * @returns A list of publications
 */
function fetchDataFromBibsonomy(mode, searchTerm, user, offset) {
	var bibSonomyFetch = new BibSonomyFetch();
	bibSonomyFetch.setLogin(getUserName(),getApiKey());

	switch (mode)
	{
	case 'own':
	default:
		return bibSonomyFetch.fetchOwn(searchTerm,offset);
	case 'user':
		return bibSonomyFetch.fetchFromUser(user,searchTerm,offset);
	case 'public':
		return bibSonomyFetch.fetchPublic(searchTerm,offset);
	}
}

/**
 * Tests if a connection to BibSonomy is possible with the current
 * login info, by fetching the profile of the current user.
 * Returns whether the user wants to display his own publications at
 * the start.
 */
function testBibsonomyConnection() {
	var bibSonomyFetch = new BibSonomyFetch();
	bibSonomyFetch.setLogin(getUserName(),getApiKey());
	bibSonomyFetch.fetchUserInfo(getUserName());
	
	return getAutoLoad() == 'true';
}

/**
 * Loads the specified document from the specified user
 * @returns Unencoded document data
 */
function fetchDocumentFromBibsonomy(user, intrahash) {
	var bibSonomyFetch = new BibSonomyFetch();
	bibSonomyFetch.setLogin(getUserName(),getApiKey());  
    return bibSonomyFetch.fetchDocument(user,intrahash);
}

/**
 * This function removes the following fields to save storage place: -
 * bibtexabstract - interhash - href
 */
function filterBibtexData(entryToFilter) {
	var newEntry = {};
	for ( var key in entryToFilter['bibtex']) {
		if (key.toLowerCase().indexOf('bibtexabstract') > -1
				|| key.toLowerCase().indexOf('interhash') > -1
				|| key.toLowerCase().indexOf('href') > -1) {
			Logger.log('Skipped: %s - %s', key, entryToFilter['bibtex'][key]);
		} else {
			Logger.log('Kept: %s - %s', key, entryToFilter['bibtex'][key]);
			newEntry[key] = entryToFilter['bibtex'][key];
		}
	}
	return newEntry;
}