/* 
 * To include this script into the GoogleDocs script editor you have to
 * add the first line to the top and the second line to the end of this file:
 * 
 * 1. <script>
 * 2. </script>
 */

/**
 * Getter for the separator.
 * 
 * @param {String}
 *            interhash - Interhash of the document to get the separator for.
 * @returns {Object} The separator.
 */
InterhashToUIMap.prototype.getSeparator = function(interhash) {
	if (this.data[interhash] === undefined)
		return undefined;
	return this.data[interhash].separator;
}

/**
 * Getter for the text.
 * 
 * @param {String}
 *            interhash - Interhash of the document to get the text for.
 * @returns {Object} The text.
 */
InterhashToUIMap.prototype.getText = function(interhash) {
	if (this.data[interhash] === undefined)
		return undefined;
	return this.data[interhash].text;
}

/**
 * Setter for the separator of a document.
 * 
 * @param {String}
 *            interhash - Interhash of the document to set the separator for.
 * @param {Object}
 *            separator - The separator for the document.
 */
InterhashToUIMap.prototype.setSeparator = function(interhash, separator) {
	if (this.data[interhash] === undefined)
		this.data[interhash] = {};
	this.data[interhash].separator = separator;
}

/**
 * Setter for the text of a document.
 * 
 * @param {String}
 *            interhash - Interhash of the document to set the text for.
 * @param {Object}
 *            separator - The separator for the document to set the text for.
 */
InterhashToUIMap.prototype.setText = function(interhash, text) {
	if (this.data[interhash] === undefined)
		this.data[interhash] = {};
	this.data[interhash].text = text;
}

/**
 * Constructor for InterhashToUIMap.
 * 
 * @returns {InterhashToUIMap} The new InterhashToUIMap object.
 */
function InterhashToUIMap() {
	this.data = {};
}